<?php
/* vim: set ts=4 expandtab: */

/* Sanity check for key param before any work */
if (!preg_match ('/^\d{14}\.\w+\.\w+\.\d{1,5}$/', $_GET['key'])) {
    die ("Invalid key.");
}

require_once "common.inc.php";
require_once "package.inc.php";

class BuildingStatus {
    // steps
    const BT_ASSIGN_TO_BBOT       = 0x000000;
    const BT_UPLOAD_TO_BHOST      = 0x000001;
    const BT_INSTALLING_BREQUIRES = 0x000002;
    const BT_BPACKAGE             = 0x000003;
    const BT_WAITING              = 0x000004;
    const BT_UPLOAD_TO_MASTER     = 0x000005;

    // status
    const BS_OK           = 0x000101;
    const BS_PROCESSING   = 0x000102;
    const BS_EMPTY        = 0x000103;
    const BS_FAILED       = 0x000104;
    const BS_NOT_QUEUED   = 0x000105;
    const BS_EXCLUDED     = 0x000106;
    const BS_PENDING      = 0x000107;
    const BS_TEMP_FAILURE = 0x000108;
    const BS_REJECTED     = 0x000109;
    
    /**
     * Package object
     *
     * @var Package
     */
    protected $package;

    /**
     * Available archs to process
     *
     * @var array
     */
    protected $archs;
    protected $directories = array(
        'done'     => '',
        'queue'    => '',
        'rejected' => ''
    );
    protected $_currentArch;
    
    /**
     * Constructor, check the required parameters to process the data.
     *
     * @param Package $package
     * @param array $required_archs
     * @param array $directories
     */
    public function __construct(Package $package, array $required_archs, array $directories) {
        $this->package = $package;

        if ($this->package->archs) {
            $this->archs = array_unique(array_merge($required_archs, $this->package->archs));
        } else {
            if (empty($required_archs)) {
                throw new Exception('No archictecture to process. Abording.');
            }
            $this->archs = $required_archs;
        }
        natsort($this->archs);
        
        
        if (!array_key_exists('donedir', $directories) ||
            !array_key_exists('queuedir', $directories) ||
            !array_key_exists('rejecteddir', $directories)
        ) {
            throw new InvalidArgumentException('Missing directory in argument 3');
        } else {
            $this->directories = $directories;
        }
    }

    /**
     *
     * @param int $step
     */
    protected function get_building_status($step) {
        if (!array_key_exists($this->archs, $this->_currentArch)) {
            return self::BS_EMPTY;
        }

        $status = $this->package->builds[$this->_currentArch][3];

        // Offset to our index
        $step--;
        switch($status[$step]) {
            case 1:
                if ($status[$step+1] != 0) {
                    return self::BS_OK;
                } elseif ($status[$step+1] != 1) {
                    return (array_key_exists($this->_currentArch,$this->package->failed)) ? self::BS_FAILED : self::BS_PROCESSING;
                }
                break;
            case 0:
                return self::BS_EMPTY;
            case 2:
                return self::BS_FAILED;
        }
    }

    /**
     *
     * @param int $step
     */
    protected function get_status($step) {
        if ($step > self::BT_ASSIGN_TO_BHOST) {
            if (self::get_status($step-1) != self::BS_OK) {
                return self::BS_EMPTY;
            }
        }
        
        if ($step == self::BT_ASSIGN_TO_BBOT) {
            if (self::get_status (self::BT_UPLOAD_TO_BHOST) != self::BS_EMPTY) {
                return self::BS_OK;
            }
            elseif (in_array ($this->_currentArch, $this->package->excluded)) {
                return self::BS_EXCLUDED;
            }
            else {
                return self::BS_PENDING;
            }
        } elseif ($step <= self::BT_BPACKAGE) {
            if (array_key_exists ($this->_currentArch, $this->package->done)) {
                return self::BS_OK;
            }
            else {
                $status = self::get_building_status ($step);
                if ($status == self::BS_FAILED && !array_key_exists ($this->_currentArch, $this->package->failed)) {
                    $status = self::BS_TEMP_FAILURE;
                }
                return $status;
            }
        } elseif ($step == self::BT_WAITING) {
            if (array_key_exists ($this->_currentArch, $this->package->done)) {
                return self::BS_OK;
            } elseif (array_key_exists ($this->_currentArch, $this->package->failed)) {
                return self::BS_FAILED;
            } elseif (in_array ($this->_currentArch, $this->package->excluded)) {
                return self::BS_EXCLUDED;
            } else {
                $status = self::get_building_status ($step-1);
                if ($status == self::BS_OK) {
                    return self::BS_PENDING;
                }
                return self::BS_EMPTY;
            }
        } elseif ($pos == self::BT_UPLOAD_TO_MASTER) {
            $prev_status = self::get_status ($step-1);
            if ($prev_status != self::BS_OK or count($this->package->failed)) {
                return self::BS_EMPTY;
            }
            if (file_exists ($this->directories['rejected'] . '/' . $this->package->base . ".youri")) {
                return self::BS_REJECTED;
            }
            if (!file_exists ($this->directories['done'] . '/' . $this->package->srpm) &&
                !file_exists ($this->directories['queue'] . '/' . $this->package->srpm)
            ) {
                foreach ($this->archs as $reqarch) {
                    if (!in_array ($reqarch, $package->archs)) {
                        return self::BS_PENDING;
                    }
                }
                return self::BS_OK;
            }
            return self::BS_PENDING;
        }
    }
    
    /**
     * Main method to call after the constructor.
     *
     * This method populate $_currentArch and call self::get_status
     * to
     *
     * @param int $step step to process
     */
    public function show_all_status($step) {
        foreach ($this->archs as $this->_currentArch) {
            echo "<td>";
            $status = self::get_status($step);
            switch ($status) {
                case self::BS_FAILED:
                    printf('<a href="/queue/failure/' . $package->base . '/">%s</a>', self::get_string($status));
                    break;
                case self::BS_REJECTED:
                    printf('<a href="/queue/rejected/' . $package->base . '/">%s</a>', self::get_string($status));
                    break;
                default:
                    if ($step == self::BT_ASSIGN_TO_BBOT &&
                        $status == self::BS_OK &&
                        array_key_exists($this->_currentArch, $this->package->builds)
                    ) {
                        printf('Host: %s', $this->package->builds[$this->_currentArch][0]);
                    } else {
                        echo self::get_string($status);
                    }
                    break;
            }
            echo "</td>";
        }
    }

    /**
     * translate the status id into a string
     *
     * @param int $status
     * @return string
     */
    static public function get_string($status) {
        switch ($status) {
            case self::BS_OK:
                return 'Ok';
            case self::BS_PROCESSING:
                return 'Processing';
            case self::BS_EMPTY:
                return '&nbsp;';
            case self::BS_FAILED:
                return 'Failed';
            case self::BS_NOT_QUEUED:
                return 'Not queued';
            case self::BS_EXCLUDED:
                return 'Excluded';
            case self::BS_PENDING:
                return 'Pending';
            case self::BS_TEMP_FAILURE:
                return 'Temporary failure';
            case self::BS_REJECTED:
                return 'Rejected';
        }
    }
    
    /**
     * Give the $archs array
     *
     * @return array
     */
    function show_archs() {
        return $this->archs;
    }
    
    public function getArchNumber() {
        return count($this->archs);
    }
}

// Build the page
page_header("package");
$pkg = new Package($_GET['key'], False);

$buildingStatus = new BuildingStatus($pkg, $requiredarchs,
    array('done' => $donedir, 'queue' => $queuedir, 'rejected' => $rejecteddir)
);

$archcount = count($archs);

?>

<center>
<h3>Build Status</h3>
<table cellspacing="8">
 <tr>
  <td><strong>Package:</strong> <?php echo $pkg->pkgname;?></td>
  <td><strong>Version:</strong> <?php echo $pkg->version;?></td>
 </tr>
 <tr>
  <td><strong>Submitter:</strong> <?php echo $pkg->submitter;?></td>
  <td><strong>Commit:</strong> <a href="http://svn.mandriva.com/cgi-bin/viewvc.cgi/packages?view=rev&revision=<?php echo $pkg->commit;?>"><?php echo $pkg->commit;?></a></td>
 </tr>
 <tr>
  <td><strong>Distro:</strong> <?php echo $pkg->distro;?></td>
  <td><strong>Repository:</strong> <?php echo $pkg->repository;?></td>
 </tr>
 <tr>
  <td><strong>Submitted from:</strong> <?php echo $pkg->submithost;?></td>
  <td><strong>Submitted on:</strong> <?php echo format_time (substr ($pkg->id, 0, 14))." GMT";?></td>
 </tr>
 <tr>
  <td colspan="2"><strong>Summary:</strong> <?php echo $pkg->summary;?></td>
 </tr>
</table>

<table cellspacing="8">
 <tr>
  <th rowspan="2">Step</th>
  <th colspan="<?php echo $buildingStatus->getArchNumber();?>">Arch</th>
 </tr>
 <tr>
<?php
foreach($buildingStatus->show_archs() as $arch) {
    echo "  <th>$arch</th>\n";
}
?>
 </tr>
 <tr>
  <td>Assigning package to a build bot</td>
  <?php echo $buildingStatus->show_all_status($buildingStatus::BT_ASSIGN_TO_BBOT);?>
 </tr>
 <tr>
  <td>Uploading to build host</td>
  <?php echo $buildingStatus->show_all_status($buildingStatus::BT_ASSIGN_TO_BHOST);?>
 </tr>
 <tr>
  <td>Installing BuildRequires</td>
  <?php echo $buildingStatus->show_all_status($buildingStatus::BT_INSTALLING_BREQUIRES);?>
 </tr>
 <tr>
  <td>Building package</td>
  <?php echo $buildingStatus->show_all_status($buildingStatus::BT_BPACKAGE);?>
 </tr>
 <tr>
  <td>Waiting for kenobi pop</td>
  <?php echo $buildingStatus->show_all_status($buildingStatus::BT_WAITING);?>
 </tr>
 <tr>
  <td>Uploading to master repository</td>
  <?php echo $buildingStatus->show_all_status($buildingStatus::BT_UPLOAD_TO_MASTER);?>
 </tr>
</table>
<p>

<?
foreach (array_keys($pkg->builds) as $arch) {
    if (array_key_exists ($arch, $pkg->failed)) {
        continue;
    }
    if ($pkg->builds[$arch][3][2] and !$pkg->builds[$arch][3][3]) {
        printf('<div style="width: 99%"><pre>Last 15 lines from build.log for %s:'."\n",$arch);
        echo $pkg->tail_buildlog($arch);
        echo "(...)\n";
        echo "</pre></div>\n";
    }
}

if (count ($pkg->done) and file_exists ("$donedir/".$pkg->srpm)) {
    echo "<table><tr><td><pre>Packages generated so far for archs";
    foreach (array_keys ($pkg->done) as $arch) {
        echo " $arch";
    }
    echo ":\n";
    @exec ("find -L $donedir -type f -name '".$_GET['key']."_*.rpm' | sed 's@^[^_]\+_@@;s/^@[0-9]\+:\(.*.src.rpm\)/\\1/'", $rpmlist);
    natsort($rpmlist);
    echo join ("\n", $rpmlist);
    echo "</pre></td></tr></table>";
}

if (count ($pkg->failed)) {
    @exec ("$script_path/analyse-rpmbuild-failures $faileddir/{$pkg->base}/log/*/build.*.log", $loglist);
    if (count ($loglist)) {
        echo "<table><tr><td><pre>Build failure reason(s):\n";
        echo join ("\n", $loglist);
        echo "</pre></td></tr></table>";
    }
}

//echo "<pre>";
//var_dump($pkg);
//echo "</pre>";
echo "</center>";
page_footer();
?>
