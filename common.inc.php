<?php

date_default_timezone_set ("GMT");

$script_path = '/var/www/bs/';
$bs_result_path = '/home/mandrake/uploads/';

/* Page creation start time */
$starttime = microtime(true);

/* BuildSystem paths */
$queuedir = "$bs_result_path/todo";
$faileddir = "$bs_result_path/failure";
$donedir = "$bs_result_path/done";
$rejecteddir = "$bs_result_path/rejected";
$builddir = "/home/mandrake/iurt";

/* Required archs for considering a package as 'built' */
$requiredarchs = array ("i586", "x86_64");
$sufficientarchs = array ("noarch");

/* SNMP configs */
$snmp_timeout = 1000000; /* miliseconds */

/*
 * format_time($timestamp)
 *   Formats a timestamp to a human-readable time
 */
function format_time($timestamp)
{
	return preg_replace("/(....)(..)(..)(..)(..)(..)/", '$1-$2-$3 $4:$5:$6', $timestamp);
}

/*
 * delta_time($timestamp)
 *   Provides a delta time from now to $timestamp.
 */
function delta_time($timestamp, $reference = '')
{
	$ftime = strptime ($timestamp, '%Y%m%d%H%M%S');
	$stamp = gmmktime (
		$ftime['tm_hour'],
		$ftime['tm_min'],
		$ftime['tm_sec'],
		$ftime['tm_mon'] + 1,
		$ftime['tm_mday'],
		$ftime['tm_year'] + 1900
	);

	if (strlen ($reference)) {
		$ftime = strptime ($reference, '%Y%m%d%H%M%S');
		$reference = gmmktime (
			$ftime['tm_hour'],
			$ftime['tm_min'],
			$ftime['tm_sec'],
			$ftime['tm_mon'] + 1,
			$ftime['tm_mday'],
			$ftime['tm_year'] + 1900
		);
	}
	else {
		$reference = gmmktime();
	}
	$delta = $reference - $stamp;
	$days = date ("z", $delta);
	$hours = date("G", $delta);
	$mins = date("i", $delta);
	$secs = date("s", $delta);
	$strdays = $days > 1 ? "days" : "day";
	$strhours = $hours > 1 ? "hours" : "hour";
	$strmins = $mins > 1 ? "minutes" : "minute";
	$strsecs = $secs > 1 ? "seconds" : "second";
	if ($days > 2) {
		return "$days $strdays";
	}
	elseif ($days) {
		return "$days $strdays $hours $strhours";
	}
	elseif ($hours) {
		return "${hours}h ${mins}min";
	}
	elseif ($mins) {
		return "${mins}min ${secs}s";
	}

	return "${secs}s";
}

/* 
 * page_header()
 *   Just like it sounds.
 */
function page_header($title)
{
?>
<html>
 <head>
  <link href="http://www.mandriva.com/archives/extension/mdkdesign/design/mdk/stylesheets/screen.css" rel="stylesheet" type="text/css">
  <link href="http://www.mandriva.com/archives/design/mdk/stylesheets/target.css.php?t=none" rel="stylesheet" type="text/css">
  <link href="http://www.mandriva.com/extension/mdkdesign/design/mdk/stylesheets/screen.css" rel="stylesheet" type="text/css">
  <link href="http://www.mandriva.com/design/mdk/stylesheets/target.css.php?t=none" rel="stylesheet" type="text/css">
  <link href="http://www.mandriva.com/extension/mdkdesign/design/mdk/stylesheets/welcome.css" rel="stylesheet" type="text/css">
  <link href="style.css" rel="stylesheet" type="text/css">
  <title>Mandriva Build System<?php ($title) ? print(" - ".$title) : "" ; ?></title>
 </head>
 <body>
  <div id="header">
   <div id="header-design">
    <div id="header-is">
     <h3 id="hide">Intersites Menu</h3>
     <ul>
      <li id="is_store"><a href="http://store.mandriva.com/"><span>Store</span></a></li>
      <li id="is_club"><a href="http://club.mandriva.com/"><span>Club</span></a></li>
      <li id="is_support"><a href="http://www.mandriva.com/en/support"><span>Support</span></a></li>
      <li id="is_downloads"><a href="http://www.mandriva.com/en/download/free"><span>Downloads</span></a></li>
      <li id="is_company"><a href="http://www.mandriva.com/enterprise/en/company"><span>Company</span></a></li>
     </ul>
     <div class="break"></div>
    </div>
    <div id="header-logo">
     <h1><a href="http://www.mandriva.com/"><span>Mandriva</span></a></h1>
     <div class="break"></div>
    </div>
    <div id="header-tools">
     <table>
      <tr>
       <td><font size=+2>Mandriva Development Server</font></td>
      </tr>
     </table>
    <div class="break"></div>
    </div>
    <div class="break"></div>
   </div>
  </div>
<p>
<?php
    navigation_bar();
    include 'motd.php';
}


/*
 * page_footer()
 *   Just like it sounds.
 */
function page_footer()
{
?>
Page generation time: 
<?php
    global $starttime;
    printf ("%5.3fs", microtime(true)-$starttime);
?>
 </body>
</html>
<?php
}


/*
 * navigation_bar()
 *   Prints the navigation bar
 */
function navigation_bar()
{
?>
<table width="99%" cols=6 cellspacing=0 border=0>
 <tr>
  <td align="left">
    <nobr><?php echo gmdate ("D, Y-m-d G:i:s T");?></nobr>
  </td>
  <!--
  <td width="20%">
   <center>
    <a href="cluster.php">Cluster overview</a>
   </center>
  </td>
  -->
  <td>
   <center>
    <a href="http://kenobi.mandriva.com/ganglia/">System Status</a>
   </center>
  </td>
  <td>
   <center>
    <a href="queue.php">Queue</a>
   </center>
  </td>
  <td>
   <center>
    <a href="build.php">Building</a>
   </center>
  </td>
  <td>
   <center>
    <a href="output.php">Output</a>
   </center>
  </td>
  <td align="right">
   <form method="get">
    <nobr>
     <center>
    Filter: <input type="text" name="filter" value="<?php echo $_GET['filter'];?>"><input type="submit" value="ok">
     </center>
    </nobr>
   </form>
  </td>
 </tr>
</table>
<?php
}


/*
 * parse_filterstring($filterstr)
 *   Parse the requested filterstr and properly sets up the globals
 *   $mustbe, $mustbenot and $oneof. Parsing this before avoids reparsing
 *   for all table cells.
 */
function parse_filterstring($filterstr)
{
	global $mustbe, $mustbenot, $oneof;
	$filterstr = $_GET['filter'];
	$words = explode(' ', $filterstr);
	$oneof = array();
	$mustbe = array();
	$mustbenot = array();
	foreach ($words as $word) {
		if ($word[0] == '-') {
			$mustbenot[] = substr($word, 1);
		}
		elseif ($word[0] == '+') {
			$mustbe[] = substr($word, 1);
		}
		else {
			$oneof[] = $word;
		}
	}
}


/*
 * filter_item($item)
 *   Match a table row against the globals $mustbe, $mustbenot and $oneof.
 *   Returns False to rows not filling the request, otherelse True.
 */
function filter_item ($item)
{
	global $mustbe, $mustbenot, $oneof;
	$foundmustbe = 0;
	$foundoneof = 0;
	foreach ($item as $element) {
		foreach ($mustbenot as $badword) {
			if (strstr($element, $badword)) {
				return False;
			}
		}
		foreach ($mustbe as $goodword) {
			if (strstr($element, $goodword)) {
				$foundmustbe++;
			}
		}
		foreach ($oneof as $goodword) {
			if (strstr($element, $goodword)) {
				$foundoneof++;
			}
		}
	}
	return (
	    ($foundmustbe == count($mustbe)) and
	    (count($oneof) ? $foundoneof > 0 : 1)
	);
}


/*
 * filter ($table)
 *   Filter the given array according to the filter expression
 *   requested via GET method.
 *   Returns the filtered table.
 */
function filter ($table)
{
	$filterstr = $_GET['filter'];
	if (!$filterstr) {
		return $table;
	}
	parse_filterstring($filterstr);
	return array_filter($table, "filter_item");
}


/*
 * parse_lock_item($item)
 *   Parses a given lock and returns a fields array
 *   $item must be like this:
 *   (base)_x86_64.iurt.frohike.20061101163646.4273.lock
 */
function parse_lock_item($item)
{
	preg_match('/_([A-Za-z0-9-_]+).iurt.(\w+)\.([0-9]{14})\.([0-9]+)/', $item, $file);
	//          i586       frohike  20061101163646 4273
	$arch = $file[1];
	if (strstr ($arch, "-noarch")) {
	    $arch = "noarch";
	}
	return array (
		$arch, // arch
		$file[2], // buildhost
		$file[3], // build start time
		$file[4] // node pid
	);
}


/*
 * parse_info($info)
 *   Parse the .info file, that contains .src.rpm informations
 */
function parse_info($item)
{
	global $queuedir;
	$contents = @file("$queuedir/$item");
	$contents = array_map ("trim", $contents);

	/*
	 * Hide epoch information.
	 *
	if ($contents[1] != "(none)") {
		$version = "${contents[1]}:${contents[2]}";
	}
	else {
		$version = "${contents[2]}";
	}
	 */
	$version = "${contents[2]}";

	preg_match('/^([^\/]+)\/([^\/]+)\/([^\/]+)\//', $item, $distrorepos);
	//              cooker      contrib       release
	preg_match('/\/([0-9]{14})\.([a-zA-Z0-9]+)\.([a-zA-Z0-9]+)/', $item, $author);
	//             20061026140631   neoclust           kenobi
	preg_match('/\.([0-9]+)_@([0-9]+):(.+)/', $item, $file);
	//             29995        @-73078       crimson..\
	if (!$file) {
		preg_match('/\.([0-9]+)_(.+)/', $item, $file);
		//             29995        @-73078       crimson..
		$file[2] = "";
	}

	return array (
		$contents[0],
		$version,
		$file[2],
		$distrorepos[1],
		"${distrorepos[2]}/${distrorepos[3]}",
		$author[2],
		$author[1],
		$file[1],
		$author[3],
		$contents[3]
		);
}

/*
 * list_info_files
 *   Search and return the list of info files
 */
function list_info_files()
{
	global $queuedir;

	@exec("find -L $queuedir -mindepth 4 -maxdepth 4 -type f -ctime -3 -name '*.src.rpm.info' |\
	    sed 's@$queuedir/*@@;".'s@.*/\([0-9]\{14\}\.\w\+\.\w\+\.[0-9]\{1,5\}\)_.*@\1 &@'."'", $rawlist);
	$infolist = array();
	foreach ($rawlist as $infoline) {
		list ($id, $infofile) = split (" ", $infoline);
		$infolist[$id] = $infofile;
	}

	$idlist = array_keys ($infolist);
	natsort ($idlist);
	return array (array_reverse ($idlist), $infolist);
}
?>
