<?php
require_once "common.inc.php";
require_once "ssh.inc.php";

class Package {
	public $pkgname;
	public $version;
	public $commit;
	public $distro;
	public $repository;
	public $submitter;
	public $id;
	public $submithost;
	public $submitpid;
	public $summary;
	public $infofile;

	// Status
	public $builds;
	public $done;
	public $failed;
	public $excluded;

	private $brief;
	private $repos_dir;
	public $base;
	public $srpm;
	private $ls_donedir;

	function __construct ($id, $brief=True, $infofile=Null) {
		$this->id = $id;
		$this->brief = $brief;
		$this->infofile = $infofile;
		$this->parse_info();
		$this->init_fs_caches();
		$this->buildtime = 0;
		$this->probe_build();
		$this->probe_done();
		$this->probe_failed();
		$this->probe_excluded();
		$this->sync_archs();
	}

	/*
	 * parse_info()
	 *   Parses .src.rpm information
	 */
	function parse_info()
	{
		global $queuedir;
		// /home/mandrake/uploads/todo/*/*/*/20061031181650.user.host.pid_*.src.rpm.info
		if (!$this->infofile) {
			@exec("find -L $queuedir -mindepth 4 -maxdepth 4 -type f -name '".
				$this->id."_*.src.rpm.info'",
				$info_list);

			if (!count($info_list)) {
				// Not found
				return;
			}

			$this->infofile = str_replace ("$queuedir", "", $info_list[0]);
			$this->infofile = ltrim ($this->infofile, "/");
		}

		$infos = parse_info($this->infofile);
		list ($this->pkgname,
			$this->version,
			$this->commit,
			$this->distro,
			$this->repository,
			$this->submitter,
			, // job id
			$this->submitpid,
			$this->submithost,
			$this->summary
		) = $infos;

		// Repos dir
		$this->repos_dir = $this->distro."/".$this->repository;

		// Base string
		$this->base = $this->repos_dir."/".$this->id;

		// src.rpm
		$this->srpm = substr ($this->infofile, 0, -5);
	}

	/*
	 * init_fs_caches()
	 *   Do some caching, avoiding too many exec calls
	 */
	function init_fs_caches()
	{
		global $donedir;
		// Cache ls donedir
		$path = "$donedir/".$this->base."_";
		$path_len = strlen ($path);
		$ls_donedir = glob ("$path*", GLOB_NOSORT);

		$this->ls_donedir = array();
		foreach ($ls_donedir as $item) {
			$item = trim ($item);
			$this->ls_donedir[] = substr ($item, $path_len);
		}
	}

	/*
	 * probe_build()
	 *   Checks if our id (timestamp) is building and properly sets:
	 *     $this->chroot
	 */
	function probe_build()
	{
		global $queuedir;
		$this->builds = array();
		// $queuedir/cooker/main/release/20061101142126.claudio.monstro.26213_i586.iurt.frohike.20061101163646.4273.lock
		$locklist = glob ("$queuedir/".$this->base."_*.lock", GLOB_NOSORT);
		$path_len = strlen ($queuedir);

		foreach ($locklist as $lockinfo) {
			$lockinfo = substr ($lockinfo, $path_len);
			$nodeinfo = $this->parse_lock($lockinfo);
			$arch = array_shift ($nodeinfo);
			$this->builds[$arch] = $nodeinfo;
		}
	}

	/*
	 * parse_lock($lockfile)
	 *   Parses a given lock file: grab the arch locked and build status if 
	 *   wanted.
	 */
	function parse_lock($lockfile) {
		$lockinfo = parse_lock_item ($lockfile);
		list ($arch,
			$buildnode,
			$buildstart,
			$pid_builder) = $lockinfo;

		if (!$this->brief) {
			$buildnodeinfo = $this->parse_buildnode($buildnode);
		}
		else {
			$buildnodeinfo = Null;
		}
		$myinfo = array (
			$arch,
			$buildnode,
			$buildstart,
			$pid_builder,
			$buildnodeinfo
		);
		return $myinfo;
	}

	/*
	 * parse_buildnode
	 *   Grabs compilation status given a buildnode.
	 */
	function parse_buildnode($buildnode)
	{
		$ssh = new SSH($buildnode);
		$ret = $ssh->connect();
		if ($ret) {
			echo "Failed to contact buildnode: code $ret.";
			return;
		}

		$build_list = $ssh->exec("find -L /export/home/mandrake/iurt/".$this->repos_dir." -type f -name '*.src.rpm' | fgrep '/".$this->id."/' | sed 's@/export/home/mandrake/iurt/*@@'");

		// /home/mandrake/iurt/*/*/*/20061031181650*/*.src.rpm
		if (!$build_list) {
			// Locked, but not building.
			echo "Locked, but not building.";
			return;
		}

		$uploaded = True;

		// Check building stage
		$logdir = '/export/home/mandrake/iurt' . "/" . dirname ($build_list[0]) . "/log";

		// Check for buildrequires stage
		// ./log/@73471:perl-POE-0.3601-2mdv2007.1.src.rpm/install_deps_@73471:perl-POE-0.3601-2mdv2007.1.src.rpm-1.0.20061106203005.log
		$buildreqfile = $logdir . "/*/install_deps*.log";
		$buildrequires = $ssh->exec("/bin/ls -sl $buildreqfile 2> /dev/null | wc -l");
		$buildrequires = (bool)$buildrequires[0];

		// real build stage
		$buildfile = $logdir . "/*/build.*.log";
		$building = $ssh->exec("/bin/ls -sl $buildfile 2> /dev/null | wc -l");
		$building = (bool)$building[0];

		// package built
		$statuslog = $logdir . "/status.log";
		$built = $ssh->exec("cut -d ' ' -f 2 $statuslog 2> /dev/null");
		$built = $built[0];
		if (count ($built) and $built != "ok") {
			if ($building) {
				$building = 2;
				$built = 0;
			}
			elseif ($buildrequires) {
				$buildrequires = 2;
				$built = 0;
			}
			elseif ($uploaded) {
				$uploaded = 2;
				$built = 0;
			}
		}
		if ($built) {
			$built = count($built);
		}
		
		return array (
			$uploaded,
			$buildrequires,
			$building,
			$built,
			$ssh,
			);
	}

	/*
	 * tail_buildlog(lines=15)
	 *   Tails the last lines from build.log:
	 *     $this->chroot
	 */
	function tail_buildlog($arch, $nlines=15)
	{
		$ssh = $this->builds[$arch][3][4];
		$buildlogfile = '/export/home/mandrake/iurt/'.$this->base.
			"/log/".$this->pkgname."*/build.*.log";
		$section = $ssh->exec("sed -n 's/^Executing(\\(.*\\)).*/\\1/p' $buildlogfile 2> /dev/null | tail -n 1");
		array_map("trim", $section);
		$section = $section[0];
		$lines = $ssh->exec("tail -n 15 $buildlogfile 2>&1");
		return "Section <b>$section</b>:\n".join ("\n", $lines);
	}

	/*
	 * probe_done
	 *   Search archs already done.
	 */
	function probe_done()
	{
		global $donedir;
		$this->done = array();
		foreach ($this->ls_donedir as $item) {
			if (!strstr ($item, ".done")) {
				continue;
			}

			$arch = str_replace (".done", "", $item);
			$filename = "$donedir/".$this->base."_$item";
			$fp = fopen ($filename, "r");
			if (!$fp) {
				echo "Failed to open .done file ($filename).";
				return;
			}
			$buildnode = fgets ($fp);
			$buildnode = substr ($buildnode, 5);
			fclose($fp);

			$this->done[$arch] = $buildnode;
			$ctime = filectime ($filename);
			if ($ctime > $this->buildtime) {
				$this->buildtime = $ctime;
			}
		}
	}

	/*
	 * probe_failed
	 *   Search archs that failed.
	 */
	function probe_failed()
	{
		global $donedir;
		$this->failed = array();
		foreach ($this->ls_donedir as $item) {
			if (!strstr ($item, ".fail")) {
				continue;
			}
			$arch = str_replace (".fail", "", $item);

			$filename = "$donedir/".$this->base."_$item";
			$fp = fopen ($filename, "r");
			if (!$fp) {
				echo "Failed to open .fail file ($filename).";
				return;
			}
			$buildnode = fgets ($fp);
			fclose($fp);
			$buildnode = substr ($buildnode, 5);

			$this->failed[$arch] = $buildnode;

			$ctime = filectime ($filename);
			if ($ctime > $this->buildtime) {
				$this->buildtime = $ctime;
			}

			$buildstart = "";
			$pid_builder = "";

			$buildnodeinfo = Null;
			if (!$this->brief) {
				$buildnodeinfo = $this->parse_failed_build_info();
			}
			$myinfo = array (
				$buildnode,
				$buildstart,
				$pid_builder,
				$buildnodeinfo
			);
			$this->builds[$arch] = $myinfo;
		}
	}

	function parse_failed_build_info()
	{
		global $faileddir;

		$uploaded = True;

		// Check building stage
		$logdir = "$faileddir/" . $this->base . "/log";

		// Check for buildrequires stage
		// ./log/@73471:perl-POE-0.3601-2mdv2007.1.src.rpm/install_deps_@73471:perl-POE-0.3601-2mdv2007.1.src.rpm-1.0.20061106203005.log
		$buildreqfile = $logdir . "/*/install_deps*.log";
		@exec("/bin/ls -sl $buildreqfile 2> /dev/null", $buildrequires);
		$buildrequires = (bool)count($buildrequires);

		// real build stage
		$buildfile = $logdir . "/*/build.*.log";
		@exec("/bin/ls -sl $buildfile 2> /dev/null", $building);
		$building = (bool)count($building);

		// package built
		$statuslog = $logdir . "/status.log";
		@exec("cut -d \\  -f 2 $statuslog 2> /dev/null", $built);
		$built = $built[0];
		if (count ($built) and $built != "ok") {
			if ($building) {
				$building = 2;
				$built = 0;
			}
			elseif ($buildrequires) {
				$buildrequires = 2;
				$built = 0;
			}
			elseif ($uploaded) {
				$uploaded = 2;
				$built = 0;
			}
		}
		if ($built) {
			$built = count($built);
		}

		return array (
			$uploaded,
			$buildrequires,
			$building,
			$built,
			Null // ssh
			);
	}

	/*
	 * probe_excluded()
	 *   Search excluded archs
	 */
	function probe_excluded()
	{
		$this->excluded = array();
		foreach ($this->ls_donedir as $item) {
			if (!strstr ($item, ".excluded")) {
				continue;
			}
			$this->excluded[] = str_replace (".excluded", "", $item);
		}
	}

	/*
	 * sync_archs()
	 *   Merge all known archs into a single list
	 */
	function sync_archs()
	{
		global $sufficientarchs, $requiredarchs;

		$this->archs = array_merge(array_keys($this->builds),
			array_keys ($this->done),
			array_keys ($this->failed),
			$this->excluded);

		foreach ($sufficientarchs as $arch) {
			if (in_array ($arch, $this->archs)) {
				$this->excluded = array_unique (array_merge ($this->excluded, $requiredarchs));
				break;
			}
		}
		$this->archs = array_merge(array_keys($this->builds),
			array_keys ($this->done),
			array_keys ($this->failed),
			$this->excluded);

		$this->archs = array_unique($this->archs);
	}
}
