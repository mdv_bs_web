<?

require_once 'common.inc.php';
require_once 'package.inc.php';

function filter_pkg($pkg)
{
	global $requiredarchs, $sufficientarchs;

	// Check if it's a queue package
	if (!(count ($pkg->done) or count ($pkg->excluded)) and !count($pkg->builds)) {
		return False;
	}
	// A failed one
	if (count($pkg->failed)) {
		return False;
	}
	// A built one
	$pkgarch = array_merge (
	    array_keys ($pkg->done),
	    array_keys ($pkg->failed),
	    $pkg->excluded
	);
	foreach ($sufficientarchs as $arch) {
		if (in_array ($arch, $pkgarch)) {
			return False;
		}
		elseif (array_key_exists ($arch, $pkg->builds)) {
			/* Building a sufficient arch */
			return True;
		}
	}
	$hit = 0;
	foreach ($requiredarchs as $arch) {
		if (in_array ($arch, $pkgarch)) {
			$hit++;
		}
	}
	if ($hit == count ($requiredarchs)) {
		return False;
	}

	return True;
}

function parse_pkg($archs, $pkg)
	{
	$pkgentry = array();
	$pkgentry[] = $pkg->pkgname;
	$pkgentry[] = $pkg->version;
	$pkgentry[] = $pkg->commit;
	$pkgentry[] = $pkg->distro;
	$pkgentry[] = $pkg->repository;
	$pkgentry[] = $pkg->submitter;
	$pkgentry[] = substr ($pkg->id, 0, 14);
	$pkgentry[] = $pkg->id;
	foreach ($archs as $arch) {
		if (array_key_exists ($arch, $pkg->done)) {
			$pkgentry[] = "<font color=\"green\">Done</font>";
		}
		elseif (array_key_exists ($arch, $pkg->failed)) {
			$pkgentry[] = "<font color=\"red\">Failed</font>";
		}
		elseif (in_array ($arch, $pkg->excluded)) {
			$pkgentry[] = "<font color=\"darkyellow\">Excluded</font>";
		}
		elseif (array_key_exists ($arch, $pkg->builds)) {
			$pkgentry[] = "<font color=\"blue\">Building/".$pkg->builds[$arch][0]."</font>";
		}
		else {
			$pkgentry[] = "Pending";
		}
	}

	return $pkgentry;
}

function parse_build()
{
	global $requiredarchs;

	list ($idlist, $infolist) = list_info_files();
	$idlist = array_reverse ($idlist);

	$pkglist = array();
	$archs = $requiredarchs;
	foreach ($idlist as $id) {
		$pkg = new Package($id, True, $infolist[$id]);
		if (filter_pkg ($pkg)) {
			$pkglist[] = $pkg;
			$archs = array_merge (
			    $archs,
			    array_keys ($pkg->done),
			    array_keys ($pkg->failed),
			    array_keys ($pkg->builds)
			);
		}
	}
	$archs = array_unique ($archs);
	natsort ($archs);

	$table = array();
	foreach ($pkglist as $pkg) {
		$pkgentry = parse_pkg($archs, $pkg);
		if (!$pkgentry) {
			continue;
		}
		$table[] = $pkgentry;
	}

	return array ($archs, $table);
}

function make_build_table ($build, $archs)
{
	$now = date('YmdHis');

	$table = "<center>
		<h3>Building</h3>
<table cellspacing=8>
 <tr>
  <th>&nbsp;</th>
  <th>Package</th>
  <th>Version</th>
  <th>Commit</th>
  <th>Distro</th>
  <th>Repository</th>
  <th>Submitter</th>
  <th>Age</th>\n";
	foreach ($archs as $arch) {
		$table .= "  <th>$arch</th>\n";
	}
	$table .= "</tr>\n";
	$pos = 0;
	foreach ($build as $line) {
		$pos++;
		$table .= " <tr ";
//		if ($now - $pkg->id > 80000) {
//			$table .= "style='background-color: darkred;'";
//		}
		if ($pos % 2 == 0) {
			$table .= "class='even'";
		}
		else {
			$table .= "class='odd'";
		}
		$table .= ">
  <td>$pos.</td>
  <td><a href=\"package.php?key=${line[7]}\">${line[0]}</a></td>
  <td align='right'>${line[1]}</td>";
		if ($line[2]) {
		    $table .= "  <td><a href=\"http://svn.mandriva.com/cgi-bin/viewvc.cgi/packages?view=rev&revision=${line[2]}\">${line[2]}</a></td>";
		}
		else {
		    $table .= "  <td>N/A</td>";
		}
		$table .= "  <td>${line[3]}</td>
  <td>${line[4]}</td>
  <td>${line[5]}</td>
  <td><nobr>".delta_time($line[6])."</nobr></td>\n";
		array_shift ($line);
		array_shift ($line);
		array_shift ($line);
		array_shift ($line);
		array_shift ($line);
		array_shift ($line);
		array_shift ($line);
		array_shift ($line);
		foreach ($line as $cell) {
			$table .= "  <td>$cell</td>\n";
		}
		$table .= " </tr>";
	}
	$table .= "</table></center>";
	return $table;
}

// Build the page
page_header("building");
list ($archs, $build) = parse_build();
$filtered = filter ($build);
$buildtable = make_build_table ($filtered, $archs);
echo $buildtable;
page_footer();
?>
