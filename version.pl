#!/usr/bin/perl
use strict;
use warnings;

use URPM;
use MDK::Common;

my %urpms;
my $rootdir = "/mnt/BIG/dis";

foreach my $f (@ARGV) {
    my ($path, $fullname) = (dirname($f), basename($f));

    my ($name, $evr) = $fullname =~ /^(.*)-([^\-]*-[^\-]*)$/ or die "bad argument $f\n";
    
    my $urpm = $urpms{$path} ||= do {
	my $urpm = URPM->new;
	my $synthesis = "$rootdir/$path/media_info/synthesis.hdlist.cz";
	my ($start, $end) = $urpm->parse_synthesis($synthesis) or die "can't parse $synthesis\n";
	$urpm;
    };

    my $status;
    if (my $pkg = srpm_by_name($urpm, $name)) {
	my $fullname = $pkg->fullname;
	my $score = $pkg->compare($evr);
	if ($score == 1) {
#	    $status = "obsolete (there is $fullname)";
	    $status = "obsolete";
	} elsif ($score == 0) {
	    $status = "available";
	} else {
#	    $status = "not yet (there is $fullname)";
	    $status = "hdlists";
	}
    } else {
	$status = "missing";
    }
    print "$fullname $status\n";
}


sub srpm_by_name {
    my ($urpm, $name) = @_;
    $urpm->{srpm_names} ||= { map { $_->name => $_ } @{$urpm->{depslist}} };

    $urpm->{srpm_names}{$name};
}
