<?php

require_once "common.inc.php";

function snmp_setup()
{
	// Stolen from a php.net comment

	// Return back the numeric OIDs, instead of text strings.
	snmp_set_oid_numeric_print(1);

	// Get just the values.
	snmp_set_quick_print(TRUE);

	// For sequence types, return just the numbers, not the string and numbers.
	snmp_set_enum_print(TRUE);

	// Don't let the SNMP library get cute with value interpretation.  This makes
	// MAC addresses return the 6 binary bytes, timeticks to return just the integer
	// value, and some other things.
	snmp_set_valueretrieval(SNMP_VALUE_PLAIN);
}

/*
 * load
 * logged in users
 * processes
 */
function server_status($server)
{
	global $snmp_timeout;

	/* Grab load */
	$load = snmpget ($server, "public", "UCD-SNMP-MIB::laLoad.1", $snmp_timeout);
	if ($load == NULL) {
		/* Host is down - timed out */
		return array ($ret, "", "", "");
	}


	/* Grab logged in users */
	$users = snmpget ($server, "public", "HOST-RESOURCES-MIB::hrSystemNumUsers.0", $snmp_timeout);

	/* Grab processes */
	$processes = snmpget ($server, "public", "HOST-RESOURCES-MIB::hrSystemProcesses.0", $snmp_timeout);

	return array (0, $users, $processes, $load);
}

/*
 * load
 * free disk space
 * logged in users
 * processes
 * enabled
 */
function node_status($buildnode)
{
	global $snmp_timeout;

	/* Grab load */
	$load = snmpget ($buildnode, "public", "UCD-SNMP-MIB::laLoad.1", $snmp_timeout);
	if ($load == NULL) {
		/* Host is down - timed out */
		return array ($ret, "", "", "", "", "");
	}

	/* Grab free disk space */
	$disk_avail = snmpget ($buildnode, "public", "UCD-SNMP-MIB::dskAvail.1", $snmp_timeout);
	$disk_total = snmpget ($buildnode, "public", "UCD-SNMP-MIB::dskTotal.1", $snmp_timeout);
	$disk = sprintf ("%dG/%dG", $disk_avail/1024/1024, $disk_total/1024/1024);

	/* Grab logged in users */
	$users = snmpget ($buildnode, "public", "HOST-RESOURCES-MIB::hrSystemNumUsers.0", $snmp_timeout);

	/* Grab processes */
	$processes = snmpget ($buildnode, "public", "HOST-RESOURCES-MIB::hrSystemProcesses.0", $snmp_timeout);

	/* Check if it's enabled */
	/* Check if cron is running */
	if (!file_exists ("/var/run/crond.pid")) {
		$enabled = FALSE;
	}
	/* Check if the node is really enabled */
	else {
		// Force always enabled, as we can't parse upload.conf anymore :(
		$enabled = TRUE;
		/*
		exec ('/bin/grep -q "^[ 	]\+'.$buildnode.'[ 	]\+=" /home/mandrake/.upload.conf',
			$output, $enabled);
		if ($enabled) {
			$enabled = FALSE;
		}
		else {
			$enabled = TRUE;
		}
		 */
	}

	return array (0, $enabled, $users, $processes, $load, $disk);
}















// Setup snmp library
snmp_setup();

// Build the page
page_header("status");

?>
<center>
<h3>Build system: servers status</h3>
<?php

/*
 * SERVERS
 */
$servers = array (
	"kenobi",
	"svn"
);

$server_status = array();

foreach ($servers as $server) {
	$server_status[$server] = server_status ($server);
}

?>
<table style='text-align: right;'>
<tr>
 <th>Server</th>
 <th>Online</th>
 <th>Users</th>
 <th>Processes</th>
 <th>Current load</th>
</tr>
<?php

foreach ($server_status as $server => $status) {
	$online = !$status[0] ? "Yes" : "No";
	echo "<tr>".
		"<th>$server</th>".
		"<td>$online</td>".
		"<td>${status[1]}</td>".
		"<td>${status[2]}</td>".
		"<td>${status[3]}</td>".
		"<tr>\n";
}

?>
</table>
<?php


/*
 * NODES
 */
$nodes = array (
	"n1",
	"n2",
	"n3",
	"n4",
	"n5",
	"celeste",
	"klodia"
);

$node_status = array();

foreach ($nodes as $node) {
	$node_status[$node] = node_status ($node);
}

?>
<br>
<h3>Build system: build nodes status</h3>
<table style='text-align: right;'>
<tr>
 <th>Node</th>
 <th>Online</th>
 <th>Enabled</th>
 <th>Users</th>
 <th>Processes</th>
 <th>Current load</th>
 <th>Disk usage</th>
</tr>
<?php

foreach ($node_status as $node => $status) {
	$online = !$status[0] ? "Yes" : "No";
	$enabled = $status[1] ? "Yes" : "No";
	echo "<tr>".
		"<th>$node</th>".
		"<td>$online</td>".
		"<td>$enabled</td>".
		"<td>${status[2]}</td>".
		"<td>${status[3]}</td>".
		"<td>${status[4]}</td>".
		"<td>${status[5]}</td>".
		"<tr>\n";
}

?>
</table>
</center>
<?php

page_footer();

?>
