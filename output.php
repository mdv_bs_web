<?

require_once 'common.inc.php';
require_once 'package.inc.php';

function parse_pkg ($pkg)
{
	global $queuedir, $rejecteddir;
	global $requiredarchs, $sufficientarchs;

	$status = '';
	if (file_exists ("$rejecteddir/".$pkg->base.".youri")) {
	    $status = 'rejected';
	}
	if (count ($pkg->failed)) {
		$status = 'failed';
	}
	if (!$status) {
		$hit = 0;
		foreach ($sufficientarchs as $arch) {
			if (array_key_exists ($arch, $pkg->done)) {
				$status = 'built';
				$hit = -1;
			}
			if (array_key_exists ($arch, $pkg->builds)) {
				# The sufficient arch is still building
				$hit = -1;
			}
		}
		if ($hit == 0) {
			# Only check required archs if sufficient ones are not used
			foreach ($requiredarchs as $arch) {
				if (array_key_exists ($arch, $pkg->done) or in_array ($arch, $pkg->excluded)) {
					$hit++;
				}
			}
			if ($hit == count ($requiredarchs)) {
				$status = 'built';
			}
		}
	}
	if (!$status) {
		return;
	}
	if ($status == 'built') {
		if (!file_exists ("$queuedir/".$pkg->srpm)) {
			$status = 'uploaded';
		}
	}

	$line = array();
	$buildtime = date('YmdHis', $pkg->buildtime);
	$starttime = substr ($pkg->id, 0, 14);
	$line[] = $status;
	$line[] = $pkg->pkgname;
	$line[] = $pkg->version;
	$line[] = $pkg->commit;
	$line[] = $pkg->distro;
	$line[] = $pkg->repository;
	$line[] = $pkg->submitter;
	$line[] = $buildtime;
	$line[] = $pkg->id;
	$line[] = delta_time ($starttime, $buildtime);

	return $line;
}

function parse_output()
{
	list ($idlist, $infolist) = list_info_files();
	$pkgs = array ();

	foreach ($idlist as $id) {
		$pkgs[] = new Package ($id, True, $infolist[$id]);
	}

	return $pkgs;
}

function buildtime_sort ($pkg_a, $pkg_b)
{
	return $pkg_b[7] - $pkg_a[7];
}

function parse_pkgs($pkgs)
{
	$pkgs = array_map ('parse_pkg', $pkgs);
	$pkgs = array_filter ($pkgs, 'count');
	usort ($pkgs, 'buildtime_sort');
	return $pkgs;
}

function check_landed($table)
{
    global $script_path;
	$pkgs = array();
	foreach ($table as $line) {
		if ($line[0] != "uploaded") {
			continue;
		}
		$pkgs[] = $line[4].'/SRPMS/'.$line[5].'/'.$line[1].'-'.$line[2];
	}

	$descriptorspec = array(
	   0 => array("file", "/dev/null", "r"),  // stdin is a file that the child will read from
	   1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
	   2 => array("file", "/dev/null", "a") // stderr is a file to write to
	);

	$pkgs = implode (' ', $pkgs);
	$process = proc_open("$script_path/version.pl $pkgs", $descriptorspec, $pipes, NULL, NULL);


	if (!is_resource($process)) {
		echo "Failed to check repository versions.";
		return $table;
	}

	$new_table = array();
	foreach ($table as $line) {
		if ($line[0] != "uploaded") {
			$new_table[] = $line;
			continue;
		}
		$output = fgets ($pipes[1]);
		$output = split (' ', $output);
		$line[0] = trim($output[1]);
		if (!strlen($line[0])) {
			$line[0] = "unknown";
		}
		$new_table[] = $line;
	}

	// It is important to close any pipes before calling
	// proc_close in order to avoid a deadlock
	fclose($pipes[1]);
	$return_value = proc_close($process);

	return $new_table;
}

/*
 * make_output_table()
 *   Create an html table based on a properly formated array.
 */
function make_output_table($output)
{
	$statustable = array (
		"failed" => "<font color=\"red\">FAILED</font>",
		"rejected" => "<font color=\"red\">REJECTED</font>",
		"built" => "<font color=\"blue\">BUILT</font>",
		"uploaded" => "<font color=\"blue\">Uploaded</font>",
		"hdlists" => "<font color=\"blue\">hdlists</font>",
		# Missing one is when a new package is submitted.
		"missing" => "<font color=\"blue\">hdlists</font>",
		"available" => "<font color=\"green\">Available</font>",
		"obsolete" => "<font color=\"green\">Obsolete</font>",
		"unknown" => "<font color=\"red\"><blink>Unknown</blink></font>"
	);
	$str = "<center>
		<h3>Output</h3>
<table cellspacing=8>
 <tr>
  <th>&nbsp;</th>
  <th>Status</th>
  <th>Package</th>
  <th>Version</th>
  <th>Commit</th>
  <th>Distro</th>
  <th>Repository</th>
  <th>Submitter</th>
  <th><nobr>Finished for</nobr></th>
  <th><nobr>Processing time</nobr></th>
 </tr>
";
	$pos = 0;
	foreach ($output as $item) {
		$pos++;
		$str .= "<tr ";
		if ($pos % 2 == 0) {
			$str .= "class='even'";
		}
		else {
			$str .= "class='odd'";
		}
		$str .= ">
  <td>$pos.</td>
  <td>".$statustable[$item[0]]."</td>
  <td><a href=\"package.php?key=${item[8]}\">${item[1]}</a></td>
  <td align='right'>${item[2]}</td>";
		if ($item[3]) {
		    $str .= "  <td><a href=\"http://svn.mandriva.com/cgi-bin/viewvc.cgi/packages?view=rev&revision=${item[3]}\">${item[3]}</a></td>";
		}
		else {
		    $str .= "<td>N/A</td>";
		}
		$str .= "  <td>${item[4]}</td>
  <td>${item[5]}</td>
  <td>${item[6]}</td>
  <td><nobr>".delta_time($item[7])."</nobr></td>
  <td><nobr>".$item[9]."</nobr></td>
 </tr>
 ";
	}
	$str .= "</table>
</center>
";
	return $str;
}

function make_stats ($pkgs)
{
	$overall = array ();
	$overall['done'] = 0;
	$overall['failed'] = 0;
	$overall['excluded'] = 0;
	$overall['total'] = 0;

	$blank = $overall;

	$h = array ();

	foreach ($pkgs as $pkg) {
		foreach (array_values ($pkg->done) as $host) {
			if (!array_key_exists($host, $h)) {
				$h[$host] = $blank;
			}
			$h[$host]['done']++;
			$h[$host]['total']++;
		}
		foreach (array_values ($pkg->failed) as $host) {
			if (!array_key_exists($host, $h)) {
				$h[$host] = $blank;
			}
			$h[$host]['failed']++;
			$h[$host]['total']++;
		}
		foreach (array_values ($pkg->excluded) as $host) {
			$overall['excluded']++;
		}
	}

	// Sum it all
	foreach (array_values ($h) as $hits) {
		$overall['done'] += $hits['done'];
		$overall['failed'] += $hits['failed'];
		$overall['total'] += $hits['total'];
	}

	$overall['total'] += $overall['excluded'];
	$h['Total'] = $overall;

	return $h;
}

function make_stats_table($stats)
{
	$str = "<center>
		<h3>Statistics (with no filtering applied)</h3>
<table cellspacing=8>
 <tr>
  <th>&nbsp;</th>
  <th>Buildnode</th>
  <th>Done</th>
  <th>Failed</th>
  <th>Excluded</th>
  <th>Total</th>
 </tr>
";
	$pos = 0;
	$keys = array_keys ($stats);
	natcasesort ($keys);
	foreach ($keys as $host) {
		$h = $stats[$host];
		$pos++;
		$str .= "<tr ";
		if ($pos % 2 == 0) {
			$str .= "class='even'";
		}
		else {
			$str .= "class='odd'";
		}
		$str .= ">
  <td>$pos.</td>
  <td>$host</td>
  <td>${h['done']}</td>
  <td>${h['failed']}</td>\n";
		if ($host == "Total") {
			$str .= "  <td>${h['excluded']}</td>\n";
		}
		else {
			$str .= "  <td>-</td>\n";
		}
		$str .= "  <td>${h['total']}</td>\n";
	}
	$str .= "</table>\n";
	return $str;
}


// Build the page
page_header("output");
$pkgs = parse_output();
$stats = make_stats ($pkgs);
$output = parse_pkgs ($pkgs);
$filtered = filter ($output);
$filtered = check_landed ($filtered);
$table = make_output_table($filtered);
$table2 = make_stats_table($stats);
echo $table2;
echo $table;
page_footer();
?>
