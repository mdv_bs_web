<?php
include 'common.inc.php';
include 'package.inc.php';


function parse()
{
	list ($idlist, $infolist) = list_info_files();
	$idlist = array_reverse ($idlist);

	$output = array();
	foreach ($idlist as $id) {
		$pkg = new Package ($id, True, $infolist[$id]);
		if (count ($pkg->failed) or // failed
			count ($pkg->done) or // done
			count ($pkg->builds) or // building
			count ($pkg->excluded) // excluded
		    ) {
			continue; // not on input queue;
		}

		$line = array();
		$line[] = $pkg->pkgname;
		$line[] = $pkg->version;
		$line[] = $pkg->commit;
		$line[] = $pkg->distro;
		$line[] = $pkg->repository;
		$line[] = $pkg->submitter;
		$line[] = substr ($pkg->id, 0, 14);
		$line[] = $pkg->id;
		$output[] = $line;
	}

	return $output;
}

/*
 * make_output_table()
 *   Create an html table based on a properly formated array.
 */
function make_queue_table($queue)
{
	$str = "<center>
		<h3>Queue</h3>
<table cellspacing=8>
 <tr>
  <th colspan=2>Package</th>
  <th>Version</th>
  <th>Commit</th>
  <th>Distro</th>
  <th>Repository</th>
  <th>Submitter</th>
  <th>Age</th>
 </tr>
";
	$pos=0;
	foreach ($queue as $item) {
		$pos++;
		$str .= " <tr ";
		if ($pos % 2 == 0) {
			$str .= "class='even'";
		}
		else {
			$str .= "class='odd'";
		}
		$str .= ">
  <td>$pos.</td>
  <td><a href=\"package.php?key=${item[7]}\">${item[0]}</a></td>
  <td align='right'>${item[1]}</td>
  ";
		if ($item[2]) {
		    $str .= "  <td><a href=\"http://svn.mandriva.com/cgi-bin/viewvc.cgi/packages?view=rev&revision=${item[2]}\">${item[2]}</a></td>";
		}
		else {
		    $str .= "  <td>N/A</td>";
		}
		$str .= "  <td>${item[3]}</td>
  <td>${item[4]}</td>
  <td>${item[5]}</td>
  <td><nobr>".delta_time($item[6])."</nobr></td>
 </tr>
";
	}
	$str .= "</table>
</center>
";

	return $str;
}

// Build the page
page_header("todo queue");
//$queue = parse_queue();
$queue = parse();
$filtered = filter ($queue);
$queuetable = make_queue_table ($filtered);
echo $queuetable;
page_footer();
?>
