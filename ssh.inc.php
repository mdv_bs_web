<?php

class SSH {
	private $connection;
	private $shell;
	private $host;
	private $cmdstart = '[cmdstartline]';
	private $cmdend = '[cmdendline]';

	function __construct($host) {
		$this->host = $host;
	}

	function connect() {
//		$this->connection = ssh2_connect($this->host, 22);
		$this->connection = ssh2_connect($this->host, 22, array('hostkey'=>'ssh-dss'));
		if (!$this->connection) {
			return 1;
		}
			/*
		if (!@ssh2_auth_password($this->connection, 'mandrake', 'mandrake')) {
			 */
		if (!@ssh2_auth_pubkey_file ($this->connection, 'mandrake',
		    '/home/mandrake/.ssh/id_dsa.pub',
		    '/home/mandrake/.ssh/id_dsa.bs')) {
			return 2;
		}
		$this->shell = ssh2_shell($this->connection, 'vanilla', array('HISTFILE' => ''));

		// Wait for the login
		$start_time = time();
		$max_time = 20; //time in seconds
		while (((time() - $start_time) < $max_time)) {
			$buf = fgets ($this->shell);
			if (strstr ($buf, '$')) {
				return 0;
			}
			usleep (100000);
		}
		// Timeout
		return 3;
	}
	
	function exec($cmd) {
		flush();
		$realcmd = "echo '".$this->cmdstart."'; ". $cmd ."; echo '".$this->cmdend."'\n";
		if (fwrite ($this->shell, $realcmd) != strlen($realcmd)) {
			return False;
		}
		flush();
		$output = "";
		$start = False;
		$start_time = time();
		$max_time = 30; //time in seconds
		while (((time() - $start_time) < $max_time)) {
			$line = fgets($this->shell);
			if (!$line) {
				# Avoid CPU 100% usage.
				usleep (100000);
				continue;
			}
			if (!strstr ($line, $cmd)) {
				if (preg_match('/'.escapeshellcmd($this->cmdstart).'/',$line)) {
					$start = True;
				}
				elseif (preg_match('/'.escapeshellcmd($this->cmdend).'/',$line)) {
					if ($output) {
						return array_map("trim", split("\n", $output));
					}
					else {
						return False;
					}
				}
				elseif ($start) {
					$output .= $line;
				}
			}
		}
	}
}

?>
